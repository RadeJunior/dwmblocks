//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"[ ", "memory", 3, 0},
	{"[ ", "disk /", 60, 0},
	{"[ ", "pacpackages", 0, 8},
	{"[ ", "weather", 3600, 0},
	{"[ ", "kbindicator", 0, 12},
	{"[ ", "volume", 0, 13},
	{"[ ", "clock",	60,	0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ']';
